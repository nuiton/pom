# Migrate

## To version 11.x

* Remove property `ciViewId`
* Fill property `gitlabGroupName`
* Fill property `gitlabProjectName` (may be migrated from `projectId`)
* Use `gitflow:release-[start|finish]` instead of `jgitflow:release-[start|finish]` for your Gitflow releases
* If your Java version is 9+, add the property `<animal.sniffer.skip>true</animal.sniffer.skip>`

## To version 10.x

* Replace property `source.skip` by `maven.source.skip` (See #3921)
* Replace property `eclispeJettyPluginVersion` by `jettyPluginVersion` (See #3925)
* Use plugin `org.eclipse.jetty:jetty-maven-plugin` instead of `org.mortbay.jetty:jetty-maven-plugin` (See #3925)

## To version 9.x

* Fill `javaVersion` (`1.6|1.7|1.8`)
* Fill `signatureArtifactId` (`java16|java17|java18`)
* Fill `signatureVersion` (`1.0` depending of the Java level)

## To version 8.x

* Fill `ciViewId` (reflect the view name in the ci)
* Remove `distributionManagement.site` (managed by pom project)

## To version 7.x

* Fill `ciViewId` (reflect the view name in the ci)
* Rename all `README.txt` files into `README.md`

## To version 6.x

* Fix Redmine URLs to use HTTPS
* Fix Nexus URLs to use HTTPS

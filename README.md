# Abstract

## Requirements

* A `Git` repository on [Nuiton's Gitlab](https://gitlab.nuiton.org)

## What can I do for you, sir?

* infrastructure configuration (redmine, ci, git)
* maven plugins versions
* release configuration
* some useful configuration in profiles

## No I don't do this! (you need to do it in your own pom.xml)

* dependencies version
* scm configuration
* site deployment at release
* svn


# How to use POM

Use the basic POM as a parent :

```xml
<parent>
  <groupId>org.nuiton</groupId>
  <artifactId>pom</artifactId>
  <version>11.16</version>
</parent>
```

If your project is part of Nuiton or Chorem, you should use the dedicated artifacts (`org.nuiton:nuitonpom` or `org.nuiton:chorempom`).

## Migrate from one version to another one

Follow the migration guide in [CHANGELOG.md](/CHANGELOG.md)

## Mandatory properties

* `gitlabGroupName` project's group name on Nuiton's Gitlab (not necessary if using `nuitonpom` or `chorempom` unless you don't use the default group)
* `gitlabProjectName` project's name on Nuiton's Gitlab
* `javaVersion` project's java compilation level (`1.6`|`1.7`|`1.8`|`9`|`10`|`11`|`12`|`13`|`14`|`15`|`16`|`17`|`18`|`19`|`20`|`21`)

If your Java version is 1.8 or lower :

* `signatureArtifactId` animal-sniffer signature artifact artifactId to check your java code level (`java16`, `java17` or `java18`)
* `signatureVersion` animal-sniffer signatures artifact  versionId (1.0)

Otherwise :

* `<animal.sniffer.skip>true</animal.sniffer.skip>`

It is not mandatory but highly recommended to have a `README.md` in each of your Maven module.

## To deploy artifacts on `Nexus`

Add in your `pom.xml`:

```xml
<maven.deploy.skip>false</maven.deploy.skip>
```

## To build javadoc

Add in your `pom.xml`:

```xml
<maven.javadoc.skip>false</maven.javadoc.skip>
```

## To build sources

Add in your `pom.xml`:

```xml
<maven.source.skip>false</maven.source.skip>
```

### Note

For projects using `nuitonpom`, `javadoc` and `sources` are included at release time, and all modules are deployed to `Nexus`.


# Do a release (on a Gitflow project)

### Initialize release

```bash
mvn gitflow:release-start
```

It will create the release branch, but won't push it unless your explicitly ask for it.

### Check before release

```bash
mvn clean verify -DperformRelease
```

If your build is clean, you can go to the next step.
Otherwise, you can fix your file project and then git the changes you made.

### Launch release

```bash
mvn gitflow:release-finish
```

### What is done?

* publish attachments in Redmine (optional)
* send a email to the commits list (optional)
* create the git merge to master
* create the git release tag
* deploy artifacts on Nexus repository

Deployment is made *AFTER* the commit/tag only if the test build is OK. You may customize the post TAG command with the property `gitflow.postReleaseGoals`. The default value is :

```xml
<gitflow.postReleaseGoals>deploy -DperformRelease -Dmaven.test.skip</gitflow.postReleaseGoals>
```

### What is not done?

* generate the maven site
* deploy the maven site


# Go further

* [Site generation/deployment](/SITE.md)
* [Frequently Asked Questions](/FAQ.md)

# Frequently Asked Questions


## How do I change the java level ?

You need to overwrite this only property :
* `javaVersion` : define the java level at compile time

If your `javaVersion` is one of `1.6`|`1.7`|`1.8`, you also need to set these properties :
* `signatureArtifactId` : the animal-sniffer signature artifactId used to check your java level code compliance
* `signatureVersion` : the animal-sniffer signature artifact's version' used to check your java level code compliance

If your `javaVersion` is one of `9`|`10`|`11`|`12`|`13`|`14`|`15` you need to disable animal-sniffer check :
* `<animal.sniffer.skip>true</animal.sniffer.skip>`


## How do I make my JAR executable ?

One way is to get all dependencies needed and tune the JAR manifest.

Here is an example of that:

```xml
  <plugin>
    <artifactId>maven-dependency-plugin</artifactId>
    <executions>
      <execution>
        <id>copy-dependencies</id>
        <goals>
          <goal>copy-dependencies</goal>
        </goals>
        <phase>prepare-package</phase>
        <configuration>
          <overWriteReleases>false</overWriteReleases>
          <overWriteSnapshots>true</overWriteSnapshots>
          <overWriteIfNewer>true</overWriteIfNewer>
          <outputDirectory>${project.build.directory}/lib</outputDirectory>
          <silent>true</silent>
        </configuration>
      </execution>
    </executions>
  </plugin>
  <plugin>
    <artifactId>maven-jar-plugin</artifactId>
    <configuration>
      <archive>
        <manifest>
          <useUniqueVersions>false</useUniqueVersions>
          <addClasspath>true</addClasspath>
          <classpathPrefix>./lib/</classpathPrefix>
          <mainClass>fqn.de.la.main.classe</mainClass>
        </manifest>
      </archive>
    </configuration>
  </plugin>
```

Another solution is to use the jar-with-dependencies assembly descriptor.

**TODO example**


## How can I do if I use dependencies that are not in Maven Central ?

By default, `POM` performs a check to forbid this. To disable this check
add this property in the property section of your pom:

```xml
<helper.skipCheckAutocontainer>true</helper.skipCheckAutocontainer>
```

Note: Using `nuitonpom`, be ware that by default the deploy repository is central. You must then change the deploy
repository by adding this configuration in the property section:

```xml
<release.repository>${other.release.repository}</release.repository>
```


## How can I make a release with a snapshot dependency (Gitflow) ?

Please note that it is NOT RECOMMENDED to do this

Just add `-DallowSnapshots=true` to the two gitflow commands:

```bash
mvn gitflow:release-start -DallowSnapshots=true
mvn gitflow:release-finish -DallowSnapshots=true
```


## How can I start a maintenance branch (Gitflow) ?

From the last tag, create a `master-x` branch, then from this branch a `develop-x` branch.

On that `develop-x` branch add in the property section this configuration:
```xml
<gitflow.masterBranchName>master-x</gitflow.masterBranchName>
<gitflow.developBranchName>develop-x</gitflow.developBranchName>
```

Note: To start a new feature think always to add the new starting develop branch:
```bash
git flow feature start myFeature develop.x
```

Release process stay the same, nothing change :).

At the end you will merge `master-x` to `master` and `develop-x` to `develop`.


## How can I shade my application JAR ?

Benefit is to get a single application JAR.

Short answer: use the [Shade Maven plugin](https://maven.apache.org/plugins/maven-shade-plugin/).

You can find an example in this project (search for `assembly-profile`): [observe application swing](https://gitlab.nuiton.org/codelutin/observe/blob/develop/application-swing/pom.xml).

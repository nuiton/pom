
# Deploy a maven site

## Using Maven

Go where you need to be on git, and launch

```bash
mvn clean verify site-deploy -DperformRelease
```

###  Where is deployed the site ?

If the project is in snapshot version, the maven site is deployed to `develop` version, otherwise it is deployed to the project version.

# Howto write a site

`POM` offers you a minimal site descriptor with common configuration, you just need to add:

* bannerLeft
* bannerRight
* links
* breadcrumbs
* menu items
* footer

## Site urls

* The site top URL is `${project.url}`. (example `http://jaxx.nuiton.org`).
* A version URL is `${project.url}/v/${siteDeployClassifier}` (example `http://jaxx.nuiton.org/v/2.8`).

`SiteDeployClassifier` represents the version of your project or `develop` if the version is a snapshot.

You don't have to figure out how to configure this property, since version 8 it is injected for you.

## Breadcrumbs

On the root module site descriptor, you need to add two breadcrumbs:
```xml
<breadcrumbs>
    <item name="${project.name}" href="${project.url}/index.html"/>
    <item name="${project.version}" href="${project.url}/v/${siteDeployClassifier}/index.html"/>
</breadcrumbs>
```

But on a child module, only one:
```xml
<item name="${project.name}" href="index.html"/>
```
## Language

In your main pom, configure it by adding the `locales` property:
```xml
<locales>fr</locales>
```

In each site descriptor, use the correct language in the footer `locale` attribute.

## Using velocity extension in your sources

Any site file can be suffixed by `.vm`, a velocity processing will be done before generating the render.

Note that any such file should be added in the `scmwebeditor_vmFiles` attribute in the footer.

## Forbid edition of some files in scmwebeditor

If you don't want to edit a file in scmwebeditor (says to hide the edit link in the develop site), then you must fill the `scmwebeditor_skipFiles` attribute in your footer.

## Footer

In the footer we define a special tag with id `mavenProjectProperties` to inject some metas of your project used later by a javascript file to render the site.

Example:

```xml
<div id='mavenProjectProperties'
     locale='fr'
     projectId='${project.projectId}'
     version='${siteDeployClassifier}'
     sourcesType='${project.siteSourcesType}'
     scmwebeditor_vmFiles=",update,install,"
     scmwebeditor_skipFiles=",index,"/>
```

* `locale` is the site locale (fr, en, ...)
* `projectId` is the project's id (in redmin)
* `version` is the site project's version (if snapshot then `develop` is used)
* `sourcesType` is the extension of format you are using in your project (default is `apt`, but could be `rst`, `md`, ...)
* `scmwebeditor_vmFiles` is a list of your files you have templatized with velocity, this is meaning that files are suffixed by `.vm`.
* `scmwebeditor_skipFiles` is a list of your read-only files to not edit in scmwebeditor (mainly report files).

Please note the formatting of the list in `scmwebeditor_vmFiles` and `scmwebeditor_skipFiles`.

## Example

You may see an [example here](https://gitlab.nuiton.org/nuiton/i18n/tree/develop/src/site).
